Name:           perl-Digest-SHA3
Version:        1.05
Release:        2
Summary:        Perl extension for SHA-3
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Digest-SHA3
Source0:        https://cpan.metacpan.org/modules/by-module/Digest/Digest-SHA3-%{version}.tar.gz

BuildRequires:  gcc coreutils findutils make perl-devel perl-generators perl-interpreter perl(Carp) perl(Config) perl(Digest::base) perl(Exporter) perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Fcntl) perl(Getopt::Std) perl(integer) perl(strict) perl(Test::More) perl(Test::Pod) >= 1.00 perl(Test::Pod::Coverage) >= 0.08 perl(vars) perl(warnings)
BuildRequires:  perl(XSLoader) sed

Requires:       perl(Carp) perl(Digest::base) perl(XSLoader)

%description
C compiler for specific processors.

%package -n     sha3sum
Summary:        Compute and check SHA3 message digest
BuildArch:      noarch
Requires:       perl(Digest::SHA3)

%description -n sha3sum
This script will compute and check the SHA3 message digest of a file

%package_help

%prep
%autosetup -n Digest-SHA3-%{version} -p1
sed -i 's|#!.*perl|#!/usr/bin/perl|' examples/dups3
chmod -c -x examples/dups3

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor OPTIMIZE="$RPM_OPT_FLAGS"
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Digest*

%files -n sha3sum
%{_bindir}/*

%files help
%{_mandir}/man1/*
%{_mandir}/man3/*
%doc README Changes examples


%changelog
* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 1.05-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jul 18 2023 leeffo <liweiganga@uniontech.com> - 1.05-1
- upgrade to version 1.05

* Wed Jun 02 2021 zhaoyao<zhaoyao32@huawei.com> - 1.04-5 
- fixs faileds: /bin/sh: gcc: command not found.

* Thu Mar 5 2020 wangye <wangye54@huawei.com> - 1.04-4
- Package init
